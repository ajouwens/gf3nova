GF3Nova - Garmin fenix 3 - Nova watchface
===============================================

Description
-----------
A minimalistic watch face.

Version history
---------------
###Version: 1.2:
+ support for D2 Bravo
+ added day of the week, day and month
###Version: 1.1
+ redesign of the hour and minute 'hands' for better readability.
###Version: 1.0:
+ minimalistic watch face with a minute and hour indicator.

Garmin forum
------------

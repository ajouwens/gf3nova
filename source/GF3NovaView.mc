using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;

class GF3NovaView extends Ui.WatchFace {
    var cx;
    var cy;
    var width;
    var height;
    var pi6 = Math.PI / 6.0;
    var pi30 = Math.PI / 30.0;
    var minuteMarkLong  = new [4];
    var is24Hour = false;

    //! Constructor
    function initialize() {
        minuteMarkLong  = [ [-2,-88], [-2,-109], [2,-109], [2,-88] ];
    }

    //! Load your resources here
    function onLayout(dc) {
        width = dc.getWidth();
        height = dc.getHeight();
        cx = width / 2;
        cy = height / 2;
        is24Hour = Sys.getDeviceSettings().is24Hour;
    }

    //! Update the view
    function onUpdate(dc) {
        var hour= Sys.getClockTime().hour;
        var min = Sys.getClockTime().min;
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_LONG);
        var dHour = hour;

        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 110);
        // Draw the minute marks
        for (var i = 0; i < 12; i += 1) {
            if (i != 3 && i != 9) {
                drawHand(dc, 5 * i * pi30, cx, cy, minuteMarkLong, Gfx.COLOR_WHITE);
            }
        }

        // Draw the minute hand
        var minUi = min * pi30;
        drawMarker(dc, minUi, 87, 22, Gfx.COLOR_RED);

        // Draw the hour hand
        hour = hour + (min / 60.0);
        hour = hour * pi6;
        drawMarker(dc, hour, 87, 16, Gfx.COLOR_BLACK);

        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 88);
        // Draw the text
        dc.drawText(cx+98, cy, Gfx.FONT_NUMBER_MILD, "3", Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(cx-98, cy, Gfx.FONT_NUMBER_MILD, "9", Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);
        // Draw the name
        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx+80, cy, Gfx.FONT_TINY, "Nova", Gfx.TEXT_JUSTIFY_RIGHT | Gfx.TEXT_JUSTIFY_VCENTER);

        var dateStr;
        if (is24Hour) {
            dateStr = Lang.format("$1$ $2$ $3$", [info.day_of_week, info.day, info.month]);
        } else {
            dHour = hour > 12 ? hour % 12 : hour;
            dateStr = Lang.format("$1$ $2$ $3$", [info.day_of_week, info.month, info.day]);
        }
        dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx-80, cy, Gfx.FONT_TINY, dateStr, Gfx.TEXT_JUSTIFY_LEFT | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function drawHand(dc, angle, centerX, centerY, coords, color) {
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + centerX;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + centerY;
            result[i] = [x, y];
        }
        dc.setColor(color, Gfx.COLOR_TRANSPARENT, color);
        dc.fillPolygon(result);
    }

    function drawMarker(dc, angle, position, z, color) {
        var coords = [ [-(z/2),-position], [0, -position - z], [(z/2), -position] ];
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + cx;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + cy;
            result[i] = [x, y];
        }
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.fillPolygon(result);
    }
}